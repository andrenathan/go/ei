[![GoDoc](https://godoc.org/gitlab.com/andrenathan/go/ei?status.svg)](https://godoc.org/gitlab.com/andrenathan/go/ei) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/andrenathan/go/ei)](https://goreportcard.com/report/gitlab.com/andrenathan/go/ei)

# ei

## Introduction

These are Go (cgo) bindings for Erlang's libei, part of [Erl_interface](http://erlang.org/doc/apps/erl_interface/index.html).
This allows for the creation of Go programs that are able to perform RPC calls against remote Erlang nodes.

## Example

```go
import (
    "log"

    "gitlab.com/andrenathan/go/ei"
)

func main() {
    node, err := ei.Connect("node", "host.example.com", "cookie")
    if err != nil {
        log.Fatal("connect failed")
    }
    defer node.Close()

    args := ei.List(
        ei.Binary("some bitstring"),
        ei.Int(1234),
    )
    result, err := ei.RPC("mymod", "myfunc", args)
    if err != nil {
        log.Fatal("rpc failed")
    }

    switch r := result {
    case ei.Tuple:
        ...
    case ei.Atom:
        ...
    }
}
```
