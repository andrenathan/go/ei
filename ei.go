// Package ei provides cgo bindings to Erlang's libei. See
// "Erl_Interface Reference Manual" for details:
// http://erlang.org/doc/apps/erl_interface/
//
// The current bindings focus on being able to call Erlang functions
// on a remote node. There is functionality to convert Go values into
// Erlang terms, perform an RPC call, and receive its result, doing
// the reverse conversion.
package ei

// #cgo LDFLAGS: -lerl_interface -lei
// #include <stdlib.h>
// #include <unistd.h>
// #include <erl_interface.h>
// #include <ei.h>
import "C"
import (
	"fmt"
	"os"
	"syscall"
	"time"
	"unsafe"
)

// Node holds information that identifies the node name, server cookie
// and file descriptor associated with a connection to another node.
type Node struct {
	cnode  C.ei_cnode
	name   *C.char
	myname *C.char
	cookie *C.char
	fd     C.int
}

// Connect creates a Go Node and sets up a connection to another
// Erlang node, identified by node@host with the given cookie.
func Connect(name string, host string, cookie string) (Node, error) {
	return ConnectTimeout(name, host, cookie, 0)
}

// ConnectTimeout is the same as Connect, but allows the specification
// of a timeout to be used in networking operations. A call to Connect
// is equivalent to calling ConnectTimeout with a timeout value of 0.
func ConnectTimeout(name string, host string, cookie string, timeout uint) (Node, error) {
	n := Node{
		cnode:  C.ei_cnode{},
		name:   C.CString(fmt.Sprintf("%v@%v", name, host)),
		myname: C.CString(fmt.Sprintf("go-ei-%d", os.Getpid())),
		cookie: C.CString(cookie),
		fd:     C.int(-1),
	}

	creat := time.Now().UnixNano()
	if ret := C.ei_connect_init(&n.cnode, n.myname, n.cookie, C.short(creat)); int(ret) != 0 {
		n.free()
		return n, syscall.Errno(C.erl_errno)
	}

	fd := C.ei_connect_tmo(&n.cnode, n.name, C.uint(timeout))
	if int(fd) < 0 {
		n.free()
		return n, syscall.Errno(int(C.erl_errno))
	}

	n.fd = fd

	return n, nil
}

// RPC performs a remote call of a function that lives in the given
// module, with the given arguments. It returns the resulting Erlang
// term or an error.
func (n *Node) RPC(mod string, fun string, args List) (Term, error) {
	buf := NewBuffer()
	defer buf.Free()

	args.Encode(buf)

	cMod := C.CString(mod)
	defer C.free(unsafe.Pointer(cMod))

	cFun := C.CString(fun)
	defer C.free(unsafe.Pointer(cFun))

	res := NewBuffer()
	defer res.Free()

	ret := C.ei_rpc(
		&n.cnode,
		n.fd,
		cMod,
		cFun,
		buf.x.buff,
		buf.x.index,
		&res.x,
	)

	if int(ret) != 0 {
		return nil, syscall.Errno(C.erl_errno)
	}

	res.x.index = 0
	return res.Decode()
}

// Close frees memory and closes the file descriptor used by the Node.
func (n *Node) Close() {
	n.free()
	if n.fd >= 0 {
		C.ei_close_connection(n.fd)
		n.fd = -1
	}
}

func (n *Node) free() {
	if n.name != nil {
		C.free(unsafe.Pointer(n.name))
		n.name = nil
	}
	if n.myname != nil {
		C.free(unsafe.Pointer(n.myname))
		n.myname = nil
	}
	if n.cookie != nil {
		C.free(unsafe.Pointer(n.cookie))
		n.cookie = nil
	}
}
