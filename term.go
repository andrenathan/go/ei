package ei

// #cgo LDFLAGS: -lerl_interface -lei
// #include <stdlib.h>
// #include <erl_interface.h>
// #include <ei.h>
import "C"
import "unsafe"

// The Term interface is satisfied by Erlang terms represented as Go
// values. It requires a single method, Encode, which encodes the
// Go value into the given Buffer so that it can be sent as an
// argument to an RPC call.
type Term interface {
	Encode(buf *Buffer)
}

// Atom encodes Go strings as Erlang atoms.
type Atom string

// Encode encodes an Atom into the given Buffer.
func (a Atom) Encode(buf *Buffer) {
	p := C.CString(string(a))
	defer C.free(unsafe.Pointer(p))
	C.ei_x_encode_atom(&buf.x, p)
}

// Binary encodes Go byte slices as Erlang binaries or bitstrings.
type Binary []byte

// Encode encodes a Binary into the given Buffer.
func (b Binary) Encode(buf *Buffer) {
	p := unsafe.Pointer(&b[0])
	C.ei_x_encode_binary(&buf.x, p, C.int(len(b)))
}

// Bool encodes Go bools as Erlang atoms 'true' or 'false'.
type Bool bool

// Encode encodes a Bool into the given Buffer.
func (b Bool) Encode(buf *Buffer) {
	p := C.int(0)
	if bool(b) {
		p = C.int(1)
	}
	C.ei_x_encode_boolean(&buf.x, p)
}

// Char encodes Go uint8 values as Erlang characters.
type Char uint8

// Encode encodes a Char into the given Buffer.
func (c Char) Encode(buf *Buffer) {
	p := C.char(c)
	C.ei_x_encode_char(&buf.x, p)
}

// Float encodes Go float64 values as Erlang doubles.
type Float float64

// Encode encodes a Float into the given Buffer.
func (x Float) Encode(buf *Buffer) {
	p := C.double(x)
	C.ei_x_encode_double(&buf.x, p)
}

// Int encodes Go int values as Erlang integers.
type Int int

// Encode encodes an Int into the given Buffer.
func (i Int) Encode(buf *Buffer) {
	p := C.long(i)
	C.ei_x_encode_long(&buf.x, p)
}

// Int64 encodes Go int64 values as Erlang integers.
type Int64 int64

// Encode encodes an Int64 into the given Buffer.
func (i Int64) Encode(buf *Buffer) {
	p := C.longlong(i)
	C.ei_x_encode_longlong(&buf.x, p)
}

// List encodes slices of terms as Erlang lists.
type List []Term

// Encode encodes a List into the given Buffer.
func (l List) Encode(buf *Buffer) {
	C.ei_x_encode_list_header(&buf.x, C.long(len(l)))
	for _, e := range l {
		e.Encode(buf)
	}
	C.ei_x_encode_empty_list(&buf.x)
}

// Map encodes Go maps from Term to Term as Erlang maps.
type Map map[Term]Term

// Encode encodes a Map into the given Buffer.
func (m Map) Encode(buf *Buffer) {
	C.ei_x_encode_map_header(&buf.x, C.long(len(m)))
	for k, v := range m {
		k.Encode(buf)
		v.Encode(buf)
	}
}

// String encodes Go strings as Erlang strings (not bitstrings).
type String string

// Encode encodes a String into the given Buffer.
func (s String) Encode(buf *Buffer) {
	p := C.CString(string(s))
	defer C.free(unsafe.Pointer(p))
	C.ei_x_encode_string(&buf.x, p)
}

// Tuple encodes slices of terms as Erlang tuples.
type Tuple []Term

// Encode encodes a Tuple into the given Buffer.
func (t Tuple) Encode(buf *Buffer) {
	C.ei_x_encode_tuple_header(&buf.x, C.long(len(t)))
	for _, e := range t {
		e.Encode(buf)
	}
}

// UInt encodes a Go uint value as an Erlang integer.
type UInt uint

// Encode encodes an UInt into the given Buffer.
func (u UInt) Encode(buf *Buffer) {
	p := C.ulong(u)
	C.ei_x_encode_ulong(&buf.x, p)
}

// UInt64 encodes a Go uint64 value as an Erlang integer.
type UInt64 uint64

// Encode encodes an UInt64 into the given Buffer.
func (u UInt64) Encode(buf *Buffer) {
	p := C.ulonglong(u)
	C.ei_x_encode_ulonglong(&buf.x, p)
}
