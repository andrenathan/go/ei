package ei

// #cgo LDFLAGS: -lerl_interface -lei
// #include <stdlib.h>
// #include <erl_interface.h>
// #include <ei.h>
import "C"
import (
	"fmt"
	"unsafe"
)

// The Buffer type holds encoded values that are sent or received
// between Erlang nodes.
type Buffer struct {
	x C.ei_x_buff
}

// NewBuffer initializes and returns a pointer to a new Buffer.
func NewBuffer() *Buffer {
	var x C.ei_x_buff
	C.ei_x_new(&x)
	return &Buffer{x}
}

// Free releases memory allocated for the Buffer.
func (b *Buffer) Free() {
	C.ei_x_free(&b.x)
}

// Decode converts the encoded Erlang term present in the Buffer
// into a Term. A type switch or type assertion can be used to
// recover the underlying Go value.
func (b *Buffer) Decode() (Term, error) {
	var typ, siz C.int
	if ret := C.ei_get_type(b.x.buff, &b.x.index, &typ, &siz); int(ret) != 0 {
		return nil, DecodeError{"buffer", int(b.x.index)}
	}

	switch typ {
	case C.ERL_ATOM_EXT:
		return b.decodeAtom(siz)
	case C.ERL_BINARY_EXT:
		return b.decodeBinary(siz)
	case C.ERL_FLOAT_EXT, C.NEW_FLOAT_EXT:
		return b.decodeFloat()
	case C.ERL_SMALL_INTEGER_EXT, C.ERL_INTEGER_EXT, C.ERL_SMALL_BIG_EXT:
		return b.decodeInt()
	case C.ERL_LIST_EXT, C.ERL_NIL_EXT:
		return b.decodeList()
	case C.ERL_MAP_EXT:
		return b.decodeMap()
	case C.ERL_STRING_EXT:
		return b.decodeString(siz)
	case C.ERL_SMALL_TUPLE_EXT, C.ERL_LARGE_TUPLE_EXT:
		return b.decodeTuple()
	default:
		return nil, DecodeError{fmt.Sprintf("unknown type %v", int(typ)), int(b.x.index)}
	}
}

func (b *Buffer) decodeAtom(length C.int) (Atom, error) {
	buf := (*C.char)(C.malloc(C.size_t(length)))
	if ret := C.ei_decode_atom(b.x.buff, &b.x.index, buf); int(ret) != 0 {
		return Atom(""), DecodeError{"atom", int(b.x.index)}
	}
	atom := Atom(C.GoStringN(buf, length))
	C.free(unsafe.Pointer(buf))
	return atom, nil
}

func (b *Buffer) decodeBinary(size C.int) (Binary, error) {
	var length C.long
	buf := C.malloc(C.size_t(size))
	if ret := C.ei_decode_binary(b.x.buff, &b.x.index, buf, &length); int(ret) != 0 {
		return Binary([]byte{}), DecodeError{"binary", int(b.x.index)}
	}
	bin := Binary(C.GoBytes(buf, C.int(length)))
	C.free(buf)
	return bin, nil
}

func (b *Buffer) decodeFloat() (Float, error) {
	var d C.double
	if ret := C.ei_decode_double(b.x.buff, &b.x.index, &d); int(ret) != 0 {
		return Float(0.0), DecodeError{"double", int(b.x.index)}
	}
	return Float(float64(d)), nil
}

func (b *Buffer) decodeInt() (Int, error) {
	var i C.long
	if ret := C.ei_decode_long(b.x.buff, &b.x.index, &i); int(ret) != 0 {
		return Int(0), DecodeError{"long", int(b.x.index)}
	}
	return Int(int(i)), nil
}

func (b *Buffer) decodeList() (List, error) {
	var arity C.int
	if ret := C.ei_decode_list_header(b.x.buff, &b.x.index, &arity); int(ret) != 0 {
		return List(nil), DecodeError{"list", int(b.x.index)}
	}

	length := int(arity)
	if length == 0 {
		return List(nil), nil
	}

	list := make([]Term, length)
	for i := 0; i < length; i++ {
		term, err := b.Decode()
		if err != nil {
			return List(nil), DecodeError{"list element", int(b.x.index)}
		}
		list[i] = term
	}
	return list, nil
}

func (b *Buffer) decodeMap() (Map, error) {
	var arity C.int

	if ret := C.ei_decode_map_header(b.x.buff, &b.x.index, &arity); int(ret) != 0 {
		return Map(nil), DecodeError{"map", int(b.x.index)}
	}

	dict := make(map[Term]Term)

	length := int(arity)
	if length == 0 {
		return Map(dict), nil
	}

	for i := 0; i < length; i++ {
		key, err := b.Decode()
		if err != nil {
			return Map(nil), DecodeError{"map key", int(b.x.index)}
		}
		value, err := b.Decode()
		if err != nil {
			return Map(nil), DecodeError{"map value", int(b.x.index)}
		}
		dict[key] = value
	}
	return Map(dict), nil
}

func (b *Buffer) decodeString(length C.int) (String, error) {
	buf := (*C.char)(C.malloc(C.size_t(length)))
	if ret := C.ei_decode_string(b.x.buff, &b.x.index, buf); int(ret) != 0 {
		return String(""), DecodeError{"string", int(b.x.index)}
	}
	str := String(C.GoStringN(buf, C.int(length)))
	C.free(unsafe.Pointer(buf))
	return str, nil
}

func (b *Buffer) decodeTuple() (Tuple, error) {
	var arity C.int
	if ret := C.ei_decode_tuple_header(b.x.buff, &b.x.index, &arity); int(ret) != 0 {
		return Tuple(nil), DecodeError{"tuple", int(b.x.index)}
	}

	length := int(arity)
	if length == 0 {
		return Tuple(nil), nil
	}

	tuple := make(Tuple, length)
	for i := 0; i < length; i++ {
		term, err := b.Decode()
		if err != nil {
			return Tuple(nil), DecodeError{"tuple element", int(b.x.index)}
		}
		tuple[i] = term
	}
	return tuple, nil
}

// DecodeError is the type of errors returned by the Decode method.
type DecodeError struct {
	what  string
	index int
}

func (e DecodeError) Error() string {
	return fmt.Sprintf("decode failed for %v at %v", e.what, e.index)
}
